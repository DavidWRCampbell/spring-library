package com.citi.training.library.rest;

import java.util.List;

import com.citi.training.library.model.Item;
import com.citi.training.library.service.ItemService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
@RequestMapping("/v1/library")
public class ItemController {
    
    private static final Logger LOG = LoggerFactory.getLogger(ItemController.class);
    
    @Autowired
    private ItemService itemService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Item> findAll(){
        LOG.debug("findAll() request recieved");
        return itemService.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Item> save(@RequestBody Item item){
        LOG.debug("save() request recieved");

        return new ResponseEntity<Item>(itemService.save(item),
                                    HttpStatus.CREATED);
    }

}
