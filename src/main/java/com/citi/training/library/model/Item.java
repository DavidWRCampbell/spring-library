package com.citi.training.library.model;

public class Item {

	
	public enum ItemType{
		BOOK,
		CD,
		DVD,
		PERIODICAL;
	}

	private String name;
    private ItemType type;
    private boolean currentlyBorrowed;
	
	public ItemType getType() {
		return type;
	}

	public void setType(ItemType type) {
		this.type = type;
	}
	
	
	public boolean isCurrentlyBorrowed() {
		return currentlyBorrowed;
	}
	public void setCurrentlyBorrowed(boolean currentlyBorrowed) {
		this.currentlyBorrowed = currentlyBorrowed;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Item [currentlyBorrowed=" + currentlyBorrowed + ", name=" + name + ", type=" + type + "]";
	}

}