package com.citi.training.library.dao;

import com.citi.training.library.model.Item;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface ItemMongoRepo extends MongoRepository<Item, String>{
    
}
