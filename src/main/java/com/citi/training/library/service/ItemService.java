package com.citi.training.library.service;

import java.util.List;

import com.citi.training.library.dao.ItemMongoRepo;
import com.citi.training.library.model.Item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
	 * This is the main business logic for item
     * 
	 * @author David
	 * @see Item
*/
@Component
public class ItemService {

    @Autowired
    private ItemMongoRepo mongoRepo;

    public List<Item> findAll(){
        return mongoRepo.findAll();
    }

    public Item save(Item item){
        return mongoRepo.save(item);
    }
    
    // public Item findBook(String testName){
    //     return mongoRepo.findOne({name: testName});
    // }
}
