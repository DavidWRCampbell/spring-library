package com.citi.training.library.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;

import com.citi.training.library.dao.ItemMongoRepo;
import com.citi.training.library.model.Item;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootTest
public class ItemServiceMockingTests {

    private static final Logger LOG = LoggerFactory.getLogger(ItemServiceMockingTests.class);

    @Autowired
    private ItemService itemService;

    @MockBean
    private ItemMongoRepo mockItemRepo;

    @Test 
    public void test_save_sanityCheck() {

        LOG.info("----------------> LOG MESSAGE");

        Item testItem = new Item();
        testItem.setType(Item.ItemType.BOOK);
        testItem.setCurrentlyBorrowed(false);
        testItem.setName("Narnia");

        when(mockItemRepo.save(testItem)).thenReturn(testItem);

        itemService.save(testItem);
    }

    @Test
    public void test_findAll_sanityCheck(){

        ArrayList<Item> testList = new ArrayList<Item>();
        Item testItem = new Item();
        testList.add(testItem);

        when(mockItemRepo.findAll()).thenReturn(testList);

        assert(itemService.findAll().size() > 0);
    }
    
}
