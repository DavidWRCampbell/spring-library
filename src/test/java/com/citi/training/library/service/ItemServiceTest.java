package com.citi.training.library.service;

import com.citi.training.library.model.Item;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ItemServiceTest {

    @Autowired
    private ItemService itemService;

    @Test
    public void test_save_sanityCheck() {
        Item testItem = new Item();
        testItem.setType(Item.ItemType.BOOK);
        testItem.setCurrentlyBorrowed(false);
        testItem.setName("Narnia");

        itemService.save(testItem);
    }

    @Test
    public void test_findAll_sanityCheck(){
        assert(itemService.findAll().size() > 0);
    }
}