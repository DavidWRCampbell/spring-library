package com.citi.training.library.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.citi.training.library.model.Item.ItemType;

import org.junit.jupiter.api.Test;

public class ItemTest {

    @Test
    public void model_Test() {

        Item testItem = new Item();
        String testName = "Top tier book";
        ItemType testItemType = ItemType.BOOK;
        Boolean testIsCurrentlyBorowed = false;
        
        testItem.setName(testName);
        assertEquals(testName, testItem.getName());

        testItem.setType(testItemType);
        assertEquals(testItemType, testItem.getType());

        testItem.setCurrentlyBorrowed(testIsCurrentlyBorowed);
        assertEquals(testIsCurrentlyBorowed, testItem.isCurrentlyBorrowed());

        testItem.toString();

    }
    
}
